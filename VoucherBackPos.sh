#!/bin/sh
#
# Autor .: Mario Sergio
# Contato: mario@rematecvarejo.com.br
function log(){
	date +"%Y-%m-%d %H:%M:%S:%3N -> $* " >> ${logdir}
}
function retirarCaracteresCnpj(){
	log "Function retirarCaracteresCnpj";
	local retorno;
	dado=$1;
	size=${#dado};
	novoDado='';

	while [ ${size} -ge 1 ];do
		n=$(echo ${dado} | cut -c ${size});
		[[ "${n}" != "." && "${n}" != "-" && "${n}" != "/" ]] && novoDado=${n}${novoDado};
		((size--));
	done
	retorno=${novoDado};
	echo ${retorno};
}
function formatarData(){
	log "Function formatarData.";
	dt=$1;
	dt=`echo ${dt} | sed -e 's/\"//g'`;
	local dataFormatado;
	a=$(echo ${dt} | cut -d ' ' -f 1);
	h=$(echo ${dt} | cut -d ' ' -f 2);
	dataFormatado="DIA "${a:8:2}"/"${a:5:2}"/"${a:0:4}" HORA ${h}"; 
	echo ${dataFormatado};
	return
}
function saldoAPagar(){
	log "Function saldoAPagar.";
	if [ ${camDes/./} -gt 0 ];then
		des=${camDes/./};
		sal=${saldo/./};
		v=$(expr ${sal} - ${des});
		saldo=$(formatarNumero ${v});
		log "Condicao if -> ${saldo}";
	elif [[ ${bestAny/./} -lt ${saldo/./} && "${bestAny/./}" != "000" ]];then
		saldo=${bestAny};
		log "Condicao elif -> ${saldo}";
	else
		log "Condicao else -> ${saldo}";
	fi
}
function payment(){
	log "Function payment."
	echo "ERROR= ";
	saldoAPagar;
	log "Saldo a Pagar = [${saldo}]";
	echo "DISPLAY Processando..."
	echo "MEDIA_TOTAL_AMOUNT=";
	echo "MEDIA_ENTERED_AMOUNT=";
	echo "MEDIA_FOUND_AMOUNT=";
	echo "MEDIA_APPROVAL=1";
	echo "MEDIA_TOTAL_AMOUNT=${valorDesconto}";
	echo "MEDIA_ENTERED_AMOUNT=${valorDesconto}";
	echo "MEDIA_FOUND_AMOUNT=${valorDesconto}";
	
	log "Saldo -> ${saldo}";
	log "Vl a pagar -> ${valorDesconto}";

	if [ "${saldo}" != "${valorDesconto}" ];then
		echo "PROMO_UPDATE_AND_RELOAD";
	fi
	echo "AT_EOS SHELL ./VoucherBackPos.sh --action=at_eos --type=payment --transacao=${transacao} --valor-pago=${valorDesconto}";
	sair ;
}
function formatarNumeroPorcento(){
	log "Function formatarNumero.";
	numero=$1;
	inteiro=$(echo ${numero} | cut -d '.' -f 1);
	local numeroFormatado;
	numeroFormatado=${inteiro};
	echo ${numeroFormatado};
	return
}
function formatarNumero(){
	log "Function formatarNumero.";
	numero=$1;
	sizeDecimal=$(echo ${numero} | cut -d '.' -f 2);
	if [ "${#sizeDecimal}" == "1" ];then
		intNumber=$(echo ${numero} | cut -d '.' -f 1);
		numero=${intNumber}"."${sizeDecimal}"0" 
	fi
	retiraCasaDecimal=$2;
	local numeroFormatado;
	numero=${numero/./};
	[ "${retiraCasaDecimal}" == "S" ] && numero=$(expr ${numero} / 10);
	sizeNumber=${#numero};
	size=0;
	numberFormat='';
	if [ ${sizeNumber} -eq 2 ];then
		numberFormat="0."${numero};
	else
		while [ ${sizeNumber} -ge 1 ];do
			if [ ${size} -eq 2 ];then
				int=$(echo ${numero} | cut -c ${sizeNumber});
				numberFormat=${int}"."${numberFormat};
				size=${#numberFormat};
				((sizeNumber--));
			else
				int=$(echo ${numero} | cut -c ${sizeNumber});
				numberFormat=${int}${numberFormat};
				size=${#numberFormat};
				((sizeNumber--));
			fi
		done
	fi
	numeroFormatado=${numberFormat};
	echo ${numeroFormatado};
	return
}
function extrairConteudoJson(){
	log "Function extrairConteudoJson";
	local retorno;
	campo=$1;
	log "Campo a ser extrair [${campo}]";
	conteudo=$(grep -Po '"'${campo}'":.*?[^\\]"' ${objeto} | grep -Po '"'${campo}'":.*?[^\\]"'|awk -F':' '{print $2}')
	size=${#conteudo};
	novoConteudo='';

	while [ ${size} -ge 1 ];do
		c=$(echo ${conteudo} | cut -c ${size});
		[ "${c}" != "\"" ] && novoConteudo=${c}${novoConteudo};
		((size--));
	done
	retorno=${novoConteudo}; 
	echo ${retorno};
}
function confirmarVoucher(){
	log "Function confirmarVoucher.";
	endPoint=${url}"/confirm";	
	log "curl --header 'Content-Type: application/json' --header  --data '${obj}' -w '%{http_code}\n' --connect-timeout ${min} --max-time ${max} -X POST ${endPoint}  --silent";
	OUTPUT=$(curl --header "Content-Type: application/json"  --data "${obj}" -w "%{http_code}\n" --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --silent);
	log "Codigo Retorno - > ${OUTPUT}";
}
function ateosPayment(){
	log "Function ateosPayment.";
	[ "${action}" == "VOID" ] && endPoint=${url}"/rollback" || endPoint=${url}"/consumer"; 
	data=$(echo -e "{\"transacao\":\""${transacao}"\",\"valorPago\": ${valorPago} }");
	log "curl --header 'Content-Type: application/json' --header  --data '${data}' -w '%{http_code}\n' --connect-timeout ${min} --max-time ${max} -X POST ${endPoint}  --silent";
	OUTPUT=$(curl --header "Content-Type: application/json"  --data "${data}" -w "%{http_code}\n" --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --silent);
	log "Codigo Retorno - > ${OUTPUT}";
}
function cancelarVoucher(){
	log "Function cancelarVoucher.";
	endPoint=${url}"/cancel";	
	log "curl --header 'Content-Type: application/json' --header  --data '${obj}' -w '%{http_code}\n' --connect-timeout ${min} --max-time ${max} -X POST ${endPoint}  --silent";
	OUTPUT=$(curl --header "Content-Type: application/json"  --data "${obj}" -w "%{http_code}\n" --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --silent);
	log "Codigo Retorno - > ${OUTPUT}";
}
function resgatarVoucherDisponivel(){
	log "Function resgatarVoucherDisponiveis.";
	sufixJson="Resgate";
	objeto=${prefixJson}${sufixJson}".json";
	data=$(echo -e "{\"filialCnpj\":\""${cnpj}"\", \"pdv\":\""${pos}"\", \"cupom\":\""${cupom}"\", \"codigo\":\""${codigo}"\", \"valorPagamento\":\""${amount}"\"}");
	log "curl --header 'Content-Type: application/json' --header  --data '${data}' -w '%{http_code}\n' --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --output ${objeto} --silent";
	OUTPUT=$(curl --header "Content-Type: application/json"  --data "${data}" -w "%{http_code}\n" --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --output ${objeto} --silent);
	log "Codigo Retorno - > ${OUTPUT}";
	retorno=$(cat ${objeto});
	log "retorno: [${retorno}]"
	statusSale=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["status"]');
	transacao=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["transacao"]');
	valorDesconto=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["valorDesconto"]');
	descricao=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["descricao"]');
	log " STATUS ................: [${statusSale}]";
	log " DESCRICAO .............: [${descricao}]";
	log " TRANSACAO .............: [${transacao}]";
	log " VALOR DESCONTO ........: [${valorDesconto}]";
}
function consultarVoucherDisponiveis(){
	log "Function consultaVoucherDisponiveis";
	sufixJson="Consulta";
	objeto=${prefixJson}${sufixJson}".json";
	data=$(echo -e "{\"filialCnpj\":\""${cnpj}"\", \"pdvFilial\":\""${pos}"\", \"cupom\":\""${cupom}"\", \"clienteCpf\":\""${cst_id}"\", \"valorCompra\":\""${subtotal}"\"}");
	log "curl --header 'Content-Type: application/json' --header  --data '${data}' -w '%{http_code}\n' --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --output ${objeto} --silent";
	OUTPUT=$(curl --header "Content-Type: application/json"  --data "${data}" -w "%{http_code}\n" --connect-timeout ${min} --max-time ${max} -X POST ${endPoint} --output ${objeto} --silent);
	log "Codigo Retorno - > ${OUTPUT}";
	retorno=$(cat ${objeto});
	log "retorno: [${retorno}]"
	totalVoucher=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["totalVoucher"]');
	statusSale=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["status"]');
	log " Total Voucher..........: [${totalVoucher}]";
	log " STATUS ................: [${statusSale}]";
}
function ger(){
	log " function ger";
	texto=$*;
	if [ ${#texto} -lt 51 ];then
		echo "$*" >> ${gerencial}
		log "$*"
	else
		log "${texto:0:50}";
		echo "${texto:0:50}" >> ${gerencial}
		log "${texto:50}";
		echo "${texto:50}" >> ${gerencial}
	fi
}
function abrirComprovante(){
	log " function abrirComprovante";
	echo "INTERNAL REPORT_NEXT_HEADER 1"
	ger "#\$GER";
}

function fecharComprovante(){
	log " function fecharComprovante";
	ger "..END_EXTRA";
	echo "INTERNAL SPOOL_PENDING 1"
}

function comprovante(){
	abrirComprovante;
	ger " ";
	ger "              COMPROVANTE VOUCHERBACK";
	ger " ";
	ger "          ----VIA CLIENTE-----";
	ger " ";
	ger "LOJA.:${loja} PDV.:${pos} CUPOM.:${cupom} ";
	ger " ";
	ger "${descricao}";
	ger " ";
	if [ "${tipoDesconto}" == "PERCENTUAL" ];then
		ger " VOCE GANHOU $(formatarNumeroPorcento ${valorDesconto}) % DE DESCONTO";
		if [[ "${valorMaximoDesconto}" != "null" && ${valorMaximoDesconto/./} -gt 0 ]];then
		        ger " LIMITADO AO VALOR MAXIMO DE R$ $(formatarNumero ${valorMaximoDesconto})"; 
		fi
	else
		ger " VOCE GANHOU R$ $(formatarNumero ${valorDesconto}) DE DESCONTO";
	fi
	ger " EM SUA PROXIMA COMPRA.";
	ger " BASTA APRESENTAR ESTE VOUCHER.";
	ger " ";
	ger " NUMERO DO VOUCHER:";
	ger " ${codigo}";
	ger " #BARCODE#${codigo}#1#";
	ger " ";
	ger " PROMACAO VALIDA ATE $(formatarData \""${fim}"\")";
	ger " ";
	fecharComprovante;
}
function adicionarVoucher(){
	log "Function adicionarVoucher.";
	if [ ${primeiroVoucher} -eq 1 ];then
		voucher=$(echo -e "{\"filialCnpj\":\""${cnpj}"\",\"clienteCpf\":\""${cst_id}"\",\"codigo\":\""${codigo}"\"}");
		primeiroVoucher=0;
	else
		voucher=$(echo -e ",{\"filialCnpj\":\""${cnpj}"\",\"clienteCpf\":\""${cst_id}"\",\"codigo\":\""${codigo}"\"}");
	fi
	voucherList=${voucherList}${voucher};
}
function amountComprovante(){
	export LC_ALL=C.UTF-8;
	primeiroVoucher=1;
	voucherList="[";
	for((ct=0;ct < ${totalVoucher};ct++));do
		descricao=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["vouchers"]['${ct}']["descricao"]'); 
		descricao=`echo ${descricao} | iconv -f UTF-8 -t ASCII//TRANSLIT`;
		codigo=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["vouchers"]['${ct}']["codigo"]'); 
		fim=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["vouchers"]['${ct}']["fim"]'); 
		tipoDesconto=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["vouchers"]['${ct}']["tipoDesconto"]'); 
		valorDesconto=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["vouchers"]['${ct}']["valorDesconto"]');
		valorMaximoDesconto=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["vouchers"]['${ct}']["valorMaximoDesconto"]');
		log "Descricao da promocao. [${descricao}]";	
		log "Codigo vocher ........ [${codigo}]";	
		log "Data termino ......... [${fim}]";	
		log "Tipo desconto ........ [${tipoDesconto}]";	
		log "Valor Desconto ....... [${valorDesconto}]";
		log "Valor Max Desconto ... [${valorMaximoDesconto}]";
		comprovante;		       
		adicionarVoucher
	done
	voucherList=${voucherList}"]";
	echo ${voucherList} > ${processVoucher};
}

function evaluate(){
	echo "EVALUATE {MEDIA_AMOUNT}"
	read amount
	echo "EVALUATE {MEDIA}"
	read media
	echo "EVALUATE {SUBTOTAL}"
	read subtotal
#	subtotal=${subtotal/./}
	echo "EVALUATE {STATUS}"
	read status
	echo "EVALUATE {WSVOUCHER}"
	read url
	url="http://192.168.15.7:8080/voucher-back/v1/voucher"
	echo "EVALUATE {STATE}"
	read state
	echo "EVALUATE {MAXTIMEOUT}"
	read max
	[[ ${max} == "MAXTIMEOUT" || ${max} -eq 0 ]] && max=30;
	echo "EVALUATE {MINTIMEOUT}"
	read min
	[[ ${min} == "MINTIMEOUT" || ${min} -eq 0 ]] && min=2;
	echo "EVALUATE {POS}"
	read pos
	echo "EVALUATE {LOJA}"
	read loja
	echo "EVALUATE {LOJA_CNPJ}"
	read cnpj
	cnpj=$(retirarCaracteresCnpj ${cnpj});
	echo "EVALUATE {CUPOM}"
	read cupom
	echo "EVALUATE {VENDA_SALDO}"
	read saldo
	echo "EVALUATE {CAMPANHA_DESCONTO}"
	read camDes
	echo "EVALUATE {BEST_ANY_MEDIA_SUBTOTAL}"
	read bestAny
	processVoucher="/var/venditor/WRK/PROCESS_VOUCHER.dat" 
	gerencial="/var/venditor/WRK/EXTRA.dat";
	dia=$(date +"%Y%m%d");
	saleDay=$(date +%y%m%d);
	prefix="/vendas/sales"
	prefixJson="../HISTORY/${saleDay}/SND/voucher"${loja}${pos}${cupom}${dia};
	versao="1.0";
	logdir="/var/log/VoucherBack.log";
	Logrotate=`cat /etc/logrotate.d/venditor | grep VoucherBack | wc -l`
	[ $Logrotate -eq 0 ] && sed -i '1i\/var\/log\/VoucherBack.log' /etc/logrotate.d/venditor;
}
function check(){
	if [[ "${url}" == "" || "${url}" == "WSVOUCHER" ]];then
		log " Url Regex nao configurado para Loja!"
		echo "MESSAGE_BOX 3 0 Url Voucher nao configurado para Loja!"
		echo "ERROR=Url Voucher configurado para Loja!";
		log "-------------------------------------------------------------------- Fim"
		echo "BYE -1";
		exit 0
	fi
}
function idVoucher(){
	log " Function idVoucher.".
	ok=0;
	flag=0;
	acao=1;
	cst_id=0;
	if [ ${cst_id} -eq 0 ]; then
		while [ ${flag} -eq 0 ]; do
			case ${acao} in
				"1")
					echo "ACCEPT TITLE Digite ou Scanneie o codigo do Voucher.";
					echo "ACCEPT PROMPT Informe:"
					echo "ACCEPT READ"
					read codigo
					log "Codigo [${codigo}]";
					if [[ "${codigo}" == "" || "${codigo}" == "0" || "${codigo}" == "NULL" ]];then
						acao=2;
					else
						ok=1;
						echo "DISPLAY Consultando...";
						flag=1;	
					fi
					;;
				"2")
					log "VOUCHER NAO INFORMADO.";
					echo "ERROR=Voucher nao identificado";
					echo "ACCEPT YESNO Voucher Invalido. Tentar Novamente?";
					read simnao
					if [ ${simnao} -eq 1 ];then
						echo "ERROR= "
						acao=1;
					else
						log " NAO DESEJA INFORMAR VOUCHER.";
						log "-------------------------------------------------------------------- Fim"
						echo "BYE -1"
						exit 0;
					fi
					;;
			esac
		done
	fi
}
function idCliente(){
	log " Function idCliente".
	ok=0;
	flag=0;
	acao=1;
	cst_id=0;
	if [ ${cst_id} -eq 0 ]; then
		while [ ${flag} -eq 0 ]; do
			case ${acao} in
				"1")
					echo "ACCEPT TITLE Solicite ao Cliente que";
					echo "ACCEPT PROMPT informe o cpf - (PINPAD)"
					echo "ACCEPT PIN_MSG Identificacao?"
					echo "ACCEPT PINDATA"
					read cst_id
					log " Identificacao [$cst_id]";
					if [[ ${cst_id} == "" || ${cst_id} == "0" || ${cst_id} == "NULL" ]];then
						acao=2;
					else
						validacpf;		
						if [ ${valid} -eq 1 ];then
							ok=1;
							echo "CPF_VOUCHER=${cst_id}";
							log "Cliente Identificado!";
							echo "MESSAGE_BOX 3 0 VOUCHER BACK! CLIENTE IDENTIFICADO!"
							echo "COMMAND 14";
							read rsl
							flag=1;	
						else
							acao=2;
						fi
					fi
					;;
				"2")
					log " CPF/CNPJ NAO INFORMADO.";
					echo "ERROR=CPF nao identificado";
					echo "ACCEPT YESNO CPF/CNPJ Invalido. Tentar Novamente?";
					read simnao
					if [ ${simnao} -eq 1 ];then
						echo "ERROR= "
						acao=1;
					else
						echo "CPF_VOUCHER= ";
						log " NAO DESEJA INFORMAR CPF/CNPJ.";
						log "-------------------------------------------------------------------- Fim"
						echo "BYE 1"
						exit 0;
					fi
					;;
			esac
		done
	fi
}
function validacpf(){
	# se for cst_id for igual a 11 caracteres considera CPF
	valid=0
	len=${#cst_id}
	if [ $len == 11 ]; then
		if [[ ${cst_id} -eq 00000000000 || ${cst_id} -eq 11111111111 || ${cst_id} -eq 22222222222 || ${cst_id} -eq 33333333333 || ${cst_id} -eq 44444444444 || ${cst_id} -eq 55555555555 || ${cst_id} -eq 66666666666 || ${cst_id} -eq 77777777777 || ${cst_id} -eq 88888888888 || ${cst_id} -eq 99999999999 ]];then
			valid=0;
		else
			mulUm=1
			DigUm=0
			for digito in {1..9}; do
				let DigUm+=$(($(echo $cst_id | cut -c$digito) * $mulUm))
				mulUm=$(echo $(($mulUm+1)))
			done
			restUm=$(($DigUm%11))
			[ $restUm -gt 9 ] && primeiroDig=0 || primeiroDig=$restUm;
			mulDois=0
			DigDois=0
			for digitonew in {1..10}; do
				let DigDois+=$(($(echo $cst_id | cut -c$digitonew) * $mulDois))
				mulDois=$(echo $(($mulDois+1)))
			done
			restDois=$(($DigDois%11))
			[ $restDois -gt 9 ] && segundoDig=0 || segundoDig=$restDois;
			if [ $(echo $cst_id | cut -c10) == $primeiroDig -a  $(echo $cst_id | cut -c11) == $segundoDig ]; then
				valid=1;
			fi
		fi
	fi
	if [ $len == 14 ]; then
		mulUm=6
		DigUm=0
		for digito in {1..12}; do
			let DigUm+=$(($(echo $cst_id | cut -c$digito) * $mulUm))
			mulUm=$(echo $(($mulUm+1)))
			[ $mulUm -eq 10 ] && mulUm=2;
		done
		restUm=$(($DigUm%11))
		[ $restUm -gt 9 ] && primeiroDig=0 || primeiroDig=$restUm;
		mulDois=5
		DigDois=0
		for digitonew in {1..13}; do
			let DigDois+=$(($(echo $cst_id | cut -c$digitonew) * $mulDois))
			mulDois=$(echo $(($mulDois+1)))
			[ $mulDois -eq 10 ] && mulDois=2;
		done
		restDois=$(($DigDois%11))
		[ $restDois -gt 9 ] && segundoDig=0 || segundoDig=$restDois;
		if [ $(echo $cst_id | cut -c13) == $primeiroDig -a  $(echo $cst_id | cut -c14) == $segundoDig ]; then
			valid=1;
		fi
	fi
}
#lendo paramentros
for (( ct=1; ct<= $#;ct++));do
	eval "prm=\${$ct}";
	P0=`echo $prm | cut -f 1 -d '='`;
	P1=`echo $prm | cut -d'=' -f2`;
	case ${P0} in
		"--action")
			action=${P1};
			;;
		"--type")
			type=${P1};
			;;
		"--transacao")
			transacao=${P1};
			;;
		"--valor-pago")
			valorPago=${P1};
			;;
	esac
done
case ${action} in
	"identify")
		evaluate
		log "-------------------------------------------------------------------- Inicio"
		log " Versao.:[$versao]      Executavel.: $0  Action.:[${action}]";
		idCliente;
		echo "BYE 1"
		exit 0;
		;;
	"end-sale")
		evaluate
		log "-------------------------------------------------------------------- Inicio"
		log " Versao.:[$versao]      Executavel.: $0  Action.:[${action}]";
		echo "EVALUATE {CPF_VOUCHER}";
	       	read cst_id;
		[[ ${#cst_id} -eq 11 && "${cst_id}" != "CPF_VOUCHER" ]] && ok=1 || ok=0;
		log "OK ->  [${ok}]";
		if [ ${ok} -eq 1 ];then
			endPoint=${url}"/consulta";	
			consultarVoucherDisponiveis;
			if [[ ! -z ${totalVoucher} && ${totalVoucher} > "0" && ${OUTPUT} -eq 200 ]]; then
				log " Total de Vocher disponivel: [${totalVoucher}]";
				log "-------------------------------------------------------------------- Fim"
				amountComprovante;
				echo "MESSAGE_BOX 3 0 VOUCHER BACK! VOCE GANHOU ${totalVoucher} VOUCHER!"
				echo "AT_EOS SHELL ./VoucherBackPos.sh --action=at_eos";
				echo "BYE 1";
				exit 0
			elif [[ ! -z ${totalVoucher} && ${totalVoucher} == "0" && ${OUTPUT} -eq 200 ]]; then
				log " Nenhum Voucher disponivel: [${totalVoucher}]";
				log "-------------------------------------------------------------------- Fim"
				[ -f ${objeto} ] && rm ${objeto};
				echo "CPF_VOUCHER= ";
				echo "BYE 1";
				exit 0
			elif [[  "${OUTPUT}" == "400" && ! -z "${statusSale}" ]]; then
				message=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["erros"][0]["mensagem"]');
				log " MESSAGE................: [${message}]";
				log "-------------------------------------------------------------------- Fim"
				echo "CPF_VOUCHER= ";
				echo "BYE 1";
				exit 0
			elif [[ ${OUTPUT} -gt 200 && ! -z "${statusSale}" ]]; then
				message=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["erros"][0]["mensagem"]');
				log " MESSAGE................: [${message}]";
				log "-------------------------------------------------------------------- Fim"
				echo "CPF_VOUCHER= ";
				echo "BYE 1";
				exit 0
			else
				log " Falha de comunicacao com o Servidor!"
				log "-------------------------------------------------------------------- Fim"
				echo "CPF_VOUCHER= ";
				echo "BYE 1";
				exit 0
			fi
		else
			echo "BYE 1";
			exit 0
		fi	 
		;;
	"payment")
		evaluate
		log "-------------------------------------------------------------------- Inicio"
		log " Versao.:[$versao]      Executavel.: $0  Action.:[${action}]";
		idVoucher;
		if [ ${ok} -eq 1 ];then
			endPoint=${url}"/resgate";
			resgatarVoucherDisponivel;
		
			if [[ ! -z ${statusSale} && ${statusSale} == "OK" && ${OUTPUT} -eq 200 ]]; then
				log " Iniciando Pagamento";
				payment;
				log "-------------------------------------------------------------------- Fim"
			#amountComprovante;
			#	echo "AT_EOS SHELL ./VoucherBackPos.sh --action=at_eos";
				echo "BYE 1";
				exit 0
			elif [[  "${OUTPUT}" == "400" && ! -z "${statusSale}" ]]; then
				message=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["erros"][0]["mensagem"]');
				log " MESSAGE................: [${message}]";
				echo "MESSAGE_BOX 3 0 [${OUTPUT}] - ${message}"
				echo "ERROR=${message}";
				log "-------------------------------------------------------------------- Fim"
				echo "BYE -1";
				exit 0
			elif [[ ${OUTPUT} -gt 200 && ! -z "${statusSale}" ]]; then
				message=$(ruby -rjson -e 'j = JSON.parse(File.read("'${objeto}'")); puts j["erros"][0]["mensagem"]');
				log " MESSAGE................: [${message}]";
				echo "MESSAGE_BOX 3 0 [${OUTPUT}] - ${message}"
				echo "ERROR=${message}";
				log "-------------------------------------------------------------------- Fim"
				echo "BYE -1";
				exit 0
			else
				log " Falha de comunicacao com o Servidor!"
				echo "MESSAGE_BOX 3 0 Falha de comunicacao com o Servidor!"
				echo "ERROR=Falha de comunicacao com o Servidor!";
				log "-------------------------------------------------------------------- Fim"
				echo "BYE -1";
				exit 0
			fi		fi

			echo "BYE 1";
			exit 0

			;;
	"confirm")
		evaluate;
		log " At_eos cash back.";
		endPoint=${url};	
		echo "EVALUATE {ACTION}"
		read action
		log " Action da venda .......: [${action}]";
		log " Json ..................: [${data}]";
		
		[ ${action} == "VOID" ] && cancelarVoucher;		
		[[ ${action} == "OK" && "${type}" == "cancel" ]] && cancelarTransacao;		
		[[ ${action} == "OK" && "${type}" == "ok" ]] && confirmarTransacao;		
		log "--------------------------------------------------------------------"
		echo "BYE 1"
		exit 0;
		;;
	"at_eos")
		evaluate;
		log " At_eos cash back.";
		log " type [$type]";
		log " transacao [$transacao].";
		endPoint=${url};	
		echo "EVALUATE {ACTION}"
		read action
		if [ -f ${processVoucher} ];then
			endPoint=${url};	
			obj=`cat ${processVoucher}`;	
			log " Action da venda .......: [${action}]";
			log " Json ..................: [${obj}]";

			[ ${action} == "VOID" ] && cancelarVoucher;		
			[ ${action} == "OK" ] && confirmarVoucher;
			rm ${processVoucher}; 
		fi		
		if [ "${type}" == "payment" ];then
			ateosPayment;
		fi
		log "--------------------------------------------------------------------"
		echo "CPF_VOUCHER= ";
		echo "BYE 1"
		exit 0;
		;;
esac

